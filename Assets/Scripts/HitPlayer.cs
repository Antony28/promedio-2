﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    PlayerLife lifeComp;

    void Start()
    {
        lifeComp = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLife>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            lifeComp.TakeDamage();
            lifeComp.SetLifeText();
        }
    }
}
