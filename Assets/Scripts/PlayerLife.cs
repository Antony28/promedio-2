﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{
    public int initialLife;
    int currentLife;
    public int enemyDmg;

    public Text lifeText; 

    void Start()
    {
        currentLife = initialLife;
        SetLifeText();
    }

    public void TakeDamage()
    {
        currentLife -= enemyDmg;
    }

    public void SetLifeText()
    {
        lifeText.text = "Vidas: " + currentLife;
        if(currentLife <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Object.Destroy(gameObject);
    }
}
