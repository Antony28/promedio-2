﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public GameObject door;
    Door compDoor;
    [SerializeField]
    bool canOpen;

    void Start()
    {
        canOpen = false;
        compDoor = door.GetComponent<Door>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && canOpen == true)
        {
            compDoor.OpenDoor();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Press E to Open");
            canOpen = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Can't Open");
            canOpen = false;
        }
    }
}
