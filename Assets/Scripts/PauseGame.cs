﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    bool pause;
    public GameObject pauseMenu;
    void Start()
    {
        pause = false;
        pauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void Pause()
    {
        pause = !pause;
        Time.timeScale = (pause) ? 0 : 1;
        pauseMenu.SetActive(pause);
    }
}